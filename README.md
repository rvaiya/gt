# Description

A command line tool to manage git repositories across different providers and accounts.

# Goals

To provide a simple tool for quickly creating private git repos 'in the
cloud' wherever that may be.

To provide emancipation from the sea of proprietary high fructose css
bullshit you don't need 99% of the time.

 - `gt init gh/project_name` bootstraps your next project.
 - `gt ls gh` lists your current github projects
 - `gt ls gl` lists your current gitlab projects
 - `gt rm gh/project_name` removes 'project_name' from github

# Installation

Requires python 3.

`pip3 install gt` should do the trick, or else
`python setup.py install` from source.

# Concept

A tool to manage personal git repos from various 'sources'. A source
might be:

 - A directory on your local file system.
 - A git service provider like gitlab or github.
 - A remote box to which you have ssh access

# Usage

Create **~/.gtrc** and populate it with sections of the following form

    [<source name>]

	type=<git source>
	<param1>=<val1>

where `<git source>` indicates the source type.

Supported sources at the time of writing include:

    - gitlab (the service)
    - github
    - ssh (An ssh-able Unix box containing a directory full of repos)

# Supported Git Sources

 - Github
   - Use the URL below to generate an API token ensuring that
     all permissions under 'repo' and 'repo_delete' are checked.

     https://github.com/settings/tokens/new

   - Add the generated token under the relevant section in your ~/.gtrc as
     'api_token'.
     
   - Note that unless you have a paid account you won't be able to create private github
     repositories using 'create' or 'init' (gitlab is recommended as a good free alternative).
     You can however still use the --public flag to create new public repos.
     
   - Ensure your local SSH key has been added to your github account  so you can push
     to new repositories you create.

 - Gitlab
   - Use the URL below to generate an API token ensuring that 'api' is checked.

     https://gitlab.com/profile/personal_access_tokens

   - Add the token under the relevant section in your ~/.gtrc as
     api_token.
     
   - Ensure your local SSH key has been added to your gitlab account  so you can push
     to new repositories you create.

 - SSH
   - Ensure key based SSH access is available for the target box (see ssh-agent(1) for
     details on multi-key management)
     
   - Designate a directory on the host to contain all git repositories correponding
     to the new host.
     
   - Set the appropriate 'host', 'user' and 'project_dir' keys in the relevant
     section of the .gtrc entry.


# Examples

## ~/.gtrc
    [ vps ]

    type=ssh
    host=dionysus
    user=rvaiya
    project_dir=/storage/projects

    [ gl ]

    type=gitlab
    api_token=<my-api-token>

    [ gh ]

    type=github
    api_token=<my-api-token>

# CLI Usage

    ~> gt ls '*'

    vps/project1
    vps/project2
    gl/projecta
    gl/projectb

    ~> mkdir projects/new_project
    ~> cd projects/new_project
    ~> gt init gl/new_project

    Creating gl/new_project

    ~> gt clone gl/new_project

    Cloning into 'new_project'...
    remote: Counting objects: 20, done.
    remote: Compressing objects: 100% (17/17), done.
    remote: Total 20 (delta 2), reused 0 (delta 0)
    Receiving objects: 100% (20/20), 22.00 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (2/2), done.
    Checking connectivity... done.

    ~> gt rm gl/newproject

    Successfully deleted gl/newproject

# Bugs/Limitations

The tool is designed to handle as many git sources as possible and so
only supports the maximal subset of common features provided by them
(i.e add/remove/delete). Since it is primarily intended for personal use
it does not support sophisticated project or permission management
features which are commonly found on proprietary collaboration platforms
like github. Such features can be managed on such platforms if they are
required after repos have been initialized by the tool.

There will be bugs.

# Extensibility

Adding a source should be as simple as creating a new class in
gt/sources which derives from gt.sources.base

The class name serves as the source type and its constructor's parameter
names are drawn from the relevant section of the config file when
instantiating the object.

It is worth noting that the project began life as a bash script and was
partly rewritten as an exercise in python by the author. As such things
may not be optimally 'pythonic' :). If you bother to read the code and
invariably feel the need to abuse the person who wrote it, know that
such sentiments will be happily received at <r.vaiya@gmail.com>.

# Feature Requests 

If anyone else actually uses this I am open to feature requests :).
